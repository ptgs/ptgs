<?php
    
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

    $postdata = file_get_contents("php://input");
    if (isset($postdata)) {
        $request = json_decode($postdata);
        $strCode = $request->kodurusan;

        if ($strCode != "") {
            
            // Need to connect to DB and fetch the Code Desc details
            $strCodeDesc = getUrusanDescByCode($strCode);
            
            echo "Nama Kod: " . $strCodeDesc;   // Display the Code Description by passing the Code
        }
        else {
            echo "Tiada Kod Urusan!";
        }
    }
    else {
        echo "Not called properly with username parameter!";
    }

    function getUrusanDescByCode($strCode) {
        
        $mysqli = new mysqli("localhost", "suk", "suk123", "suk_perserahan");

        $query = "SELECT * FROM urusan WHERE kod = '$strCode'"; 

        $dbresult = $mysqli->query($query);
        
        while($row = $dbresult->fetch_array(MYSQLI_ASSOC)){
        
            $data[] = array(
                'urusan' => $row['urusan']
            );
        }
        
        if($dbresult){
            $result = "{'success':true, 'data':" . json_encode($data) . "}";             
        }
        else {
            $result = "{'success':false}";
        }
        
        return $result;

     }
?>