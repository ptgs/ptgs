import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule} from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AduanPage } from '../pages/aduan/aduan';
import { NoPerserahanPage } from '../pages/no-perserahan/no-perserahan';
import { NoHakmilikPage } from '../pages/no-hakmilik/no-hakmilik';
import { PungutanPage } from '../pages/pungutan/pungutan';
import { ResultPage } from '../pages/result/result';
import { PerserahanPage } from '../pages/perserahan/perserahan';
import { ResultPerserahanPage } from '../pages/result-perserahan/result-perserahan'; 
//import { SqlitehelperProvider } from '../providers/sqlitehelper/sqlitehelper';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AduanPage,
    NoPerserahanPage,
    NoHakmilikPage,
    PungutanPage,
    ResultPage,
    PerserahanPage,
    ResultPerserahanPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AduanPage,
    NoPerserahanPage,
    NoHakmilikPage,
    PungutanPage,
    ResultPage,
    PerserahanPage,
    ResultPerserahanPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    //SqlitehelperProvider
  ]
})
export class AppModule {}
