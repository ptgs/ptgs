import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http


/**
 * Generated class for the AduanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-aduan',
  templateUrl: 'aduan.html',
})
export class AduanPage {
  [x: string]: any;
  data:any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,public http: Http) {
      this.data.kp = '';
      // this.data.email = '';
      
      this.http = http;
  }

  ionViewDidLoad() {
      console.log('ionViewDidLoad AduanPage');
  }

  submit() {
      //var link = 'http://system.hpcs.my/ptgs/api.php';
      var link = 'http://system.hpcs.my/ptgs/getAduan.php';
      var myKP = JSON.stringify({kp: this.data.kp});
      // var myMail = JSON.stringify({email: this.data.email});
      
      this.http.post(link, myKP) // , myMail
      .subscribe(data => {
          this.data.response = data["_body"]; //https://stackoverflow.com/questions/39574305/property-body-does-not-exist-on-type-response
      }, error => {
          console.log("Oooops!");
      });
  }

}
