import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoHakmilikPage } from './no-hakmilik';

@NgModule({
  declarations: [
    NoHakmilikPage,
  ],
  imports: [
    IonicPageModule.forChild(NoHakmilikPage),
  ],
})
export class NoHakmilikPageModule {}
