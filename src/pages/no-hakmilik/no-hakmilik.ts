import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { ResultPage } from '../result/result';

/**
 * Generated class for the NoHakmilikPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-no-hakmilik',
  templateUrl: 'no-hakmilik.html',
})
export class NoHakmilikPage {
  data:any = {};
  

    constructor(public navCtrl: NavController, public http: Http, ) {
    this.data.nohak = '';
    this.http = http;
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NoHakmilikPage');
  }
  resultPage(){
    this.navCtrl.push (ResultPage);
   }
}
