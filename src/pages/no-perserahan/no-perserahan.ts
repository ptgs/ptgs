import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { ResultPage } from '../result/result';
/**
 * Generated class for the NoPerserahanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-no-perserahan',
  templateUrl: 'no-perserahan.html',
})
export class NoPerserahanPage {
  data:any = {};
  

    constructor(public navCtrl: NavController, public http: Http, ) {
    this.data.noserah = '';
    this.http = http;
    }
 ionViewDidLoad() {
  console.log('ionViewDidLoad NoPerserahanPage');
  }

  resultPage(){
    this.navCtrl.push (ResultPage);
   }

}

