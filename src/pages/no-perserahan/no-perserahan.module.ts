import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoPerserahanPage } from './no-perserahan';

@NgModule({
  declarations: [
    NoPerserahanPage,
  ],
  imports: [
    IonicPageModule.forChild(NoPerserahanPage),
  ],
})
export class NoPerserahanPageModule {}
