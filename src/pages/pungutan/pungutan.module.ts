import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PungutanPage } from './pungutan';

@NgModule({
  declarations: [
    PungutanPage,
  ],
  imports: [
    IonicPageModule.forChild(PungutanPage),
  ],
})
export class PungutanPageModule {}
