import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NoPerserahanPage } from '../no-perserahan/no-perserahan';
import { NoHakmilikPage } from '../no-hakmilik/no-hakmilik';

/**
 * Generated class for the PungutanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pungutan',
  templateUrl: 'pungutan.html',
})
export class PungutanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PungutanPage');
  }
  
  perserahanPage(){
    this.navCtrl.push (NoPerserahanPage);
   }

   hakmilikPage(){
    this.navCtrl.push (NoHakmilikPage);
   }
}
