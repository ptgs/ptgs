import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResultPerserahanPage } from '../result-perserahan/result-perserahan';

/**
 * Generated class for the PerserahanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perserahan',
  templateUrl: 'perserahan.html',
})
export class PerserahanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerserahanPage');
  }

  perserahanPage(){
    this.navCtrl.push (ResultPerserahanPage);
   }

}
