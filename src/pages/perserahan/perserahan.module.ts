import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerserahanPage } from './perserahan';

@NgModule({
  declarations: [
    PerserahanPage,
  ],
  imports: [
    IonicPageModule.forChild(PerserahanPage),
  ],
})
export class PerserahanPageModule {}
