import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http'; //https://stackoverflow.com/questions/43609853/angular-4-and-ionic-3-no-provider-for-http
import { PungutanPage } from '../pungutan/pungutan';
import { PerserahanPage } from '../perserahan/perserahan';
import { AduanPage } from '../aduan/aduan';

@Component({
 	selector: 'page-home',
  	templateUrl: 'home.html'
})

export class HomePage {
    data:any = {};

    constructor(public navCtrl: NavController, public http: Http) {
        //this.data.username = '';
        this.data.kodurusan = '';
        this.data.response = '';

        this.http = http;
    }

    submit() {
        //var link = 'http://system.hpcs.my/ptgs/api.php';
        var link = 'http://localhost/ptgs/getUrusanDesc.php';
        var myData = JSON.stringify({kodurusan: this.data.kodurusan});
       // var myData = JSON.stringify({username: this.data.username});
        
        this.http.post(link, myData)
        .subscribe(data => {
        	this.data.response = data["_body"]; //https://stackoverflow.com/questions/39574305/property-body-does-not-exist-on-type-response
        }, error => {
            console.log("Oooops!");
        });
    }
  
    nextPage(){
        this.navCtrl.push (PungutanPage);
    }
 
    aduanPage(){
        this.navCtrl.push (AduanPage);
    }


     perserahanPage(){
        this.navCtrl.push (PerserahanPage);
    }
}
