import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PerserahanPage } from '../perserahan/perserahan';
import { HomePage } from '../home/home';

/**
 * Generated class for the ResultPerserahanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-result-perserahan',
  templateUrl: 'result-perserahan.html',
})
export class ResultPerserahanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultPerserahanPage');
  }

  mainPage(){
    this.navCtrl.push (HomePage);
   }

   perserahanPage(){
    this.navCtrl.push (PerserahanPage);
   }
}
