import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ResultPerserahanPage } from './result-perserahan';

@NgModule({
  declarations: [
    ResultPerserahanPage,
  ],
  imports: [
    IonicPageModule.forChild(ResultPerserahanPage),
  ],
})
export class ResultPerserahanPageModule {}
